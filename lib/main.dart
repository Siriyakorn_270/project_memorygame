import 'package:flutter/material.dart';
import 'package:memory_game/homepage.dart';
import 'package:flip_card/flip_card.dart';
import 'package:memory_game/login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Login(),
    );
  }
}
