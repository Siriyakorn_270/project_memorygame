import 'package:flip_card/flip_card.dart';
import 'package:flutter/cupertino.dart';

enum Level { Hard, Medium, Easy }

List<String> fillsourceArray() {
  return [
    'assets/dino.png',
    'assets/dino.png',
    'assets/wolf.png',
    'assets/wolf.png',
    'assets/peacock.png',
    'assets/peacock.png',
    'assets/whale.png',
    'assets/whale.png',
    'assets/octo.png',
    'assets/octo.png',
    'assets/fish.png',
    'assets/fish.png',
    'assets/frog.png',
    'assets/frog.png',
    'assets/seahorse.png',
    'assets/seahorse.png',
    'assets/girraf.png',
    'assets/girraf.png',
  ];
}

List<String> getSourceArray(Level level) {
  List<String> levellist = <String>[];
  List sourceArray = fillsourceArray();
  if (level == Level.Hard) {
    sourceArray.forEach((element) {
      levellist.add(element);
    });
  } else if (level == Level.Medium) {
    for (int i = 0; i < 12; i++) {
      levellist.add(sourceArray[i]);
    }
  } else if (level == Level.Easy) {
    for (int i = 0; i < 6; i++) {
      levellist.add(sourceArray[i]);
    }
  }
  levellist.shuffle();
  return levellist;
}

List<bool> getInitialitemState(Level level) {
  List<bool> initialitemState = <bool>[];
  if (level == Level.Hard) {
    for (int i = 0; i < 18; i++) {
      initialitemState.add(true);
    }
  } else if (level == Level.Medium) {
    for (int i = 0; i < 12; i++) {
      initialitemState.add(true);
    }
  } else if (level == Level.Easy) {
    for (int i = 0; i < 6; i++) {
      initialitemState.add(true);
    }
  }
  return initialitemState;
}

List<GlobalKey<FlipCardState>> getCardStateKeys(Level level) {
  List<GlobalKey<FlipCardState>> cardStateKeys = <GlobalKey<FlipCardState>>[];
  if (level == Level.Hard) {
    for (int i = 0; i < 18; i++) {
      cardStateKeys.add(GlobalKey<FlipCardState>());
    }
  } else if (level == Level.Medium) {
    for (int i = 0; i < 12; i++) {
      cardStateKeys.add(GlobalKey<FlipCardState>());
    }
  } else if (level == Level.Easy) {
    for (int i = 0; i < 6; i++) {
      cardStateKeys.add(GlobalKey<FlipCardState>());
    }
  }
  return cardStateKeys;
}
